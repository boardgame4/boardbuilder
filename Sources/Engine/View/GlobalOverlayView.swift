//
//  SwiftUIView.swift
//  
//
//  Created by Arnaud VERRIER on 02/03/2024.
//

import Combine
import SwiftUI

public struct GlobalOverlayView<Overlays: View>: View {
    let cursorService = CursorEnvironnementService()
    
    let actionService: ActionnerEnvironnementService
    
    let overlays: () -> Overlays
    
    let restrictionZone: ((CGRect) -> CGRect)?
    
    /// Creates a page with the provided overlays.
    /// - Parameter overlayList: The overlays to display in the page.
    public init(
        actionnerRegistrar: ActionnerRegistrar,
        restrictionZone: ((CGRect) -> CGRect)? = nil,
        @ViewBuilder overlayList: @escaping () -> Overlays
    ) {
        self.overlays = overlayList
        self.actionService = ActionnerEnvironnementService(
           actionnerRegistrar: actionnerRegistrar
        )
        self.restrictionZone = restrictionZone
    }
    
    public var body: some View {
        ZStack {
            overlays()
                .environmentObject(cursorService)
                .environmentObject(actionService)
            CursorOverlayView(
                cursorService: cursorService,
                restrictionZone: restrictionZone
            )
        }
    }
}

public struct CursorOverlayView: View {
    @StateObject
    var cursorActionner: CursorActionner
    
    let restrictionZone: ((CGRect) -> CGRect)?
    
    init(cursorService: CursorEnvironnementService, restrictionZone: ((CGRect) -> CGRect)?) {
        self._cursorActionner = .init(wrappedValue: cursorService.registerActionner())
        self.restrictionZone = restrictionZone
    }
    
    public var body: some View {
        GeometryReader() { geometry in
            ZStack {
                Rectangle()
                    .fill(.white)
                    .opacity(0.01)
                    .simultaneousGesture(
                        DragGesture(minimumDistance: 0)
                            .onChanged({ value in
                                if let restrictionZone {
                                    let zone = restrictionZone(geometry.frame(in: .global))
                                    guard zone.contains(value.location) else {
                                        return
                                    }
                                }
                                cursorActionner.cursorLocation = value.location
                            })
                            .onEnded({ _ in
                                cursorActionner.cursorLocation = nil
                            })
                    )
                if let coordinate = cursorActionner.cursorLocation {
                    Circle()
                        .fill(.green)
                        .opacity(0.8)
                        .frame(width: 50, height: 50)
                        .position(coordinate)
                }
            }
        }
    }
}
