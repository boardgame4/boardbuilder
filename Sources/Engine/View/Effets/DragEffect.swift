//
//  SelectEffect.swift
//
//
//  Created by Arnaud VERRIER on 10/03/2024.
//

import Foundation
import SwiftUI

public struct DragEffect: ViewModifier {
    public var action: RecievedCursorAction
    
    public init(action: RecievedCursorAction) {
        self.action = action
    }

    public func body(content: Content) -> some View {
        content
            .rotation3DEffect(.degrees(action == .none ? 0 : 180), axis: (x: 0, y: 1, z: 0))
            .animation(.easeInOut(duration: action == .none ? 1 : 0.2), value: action)
    }
}
