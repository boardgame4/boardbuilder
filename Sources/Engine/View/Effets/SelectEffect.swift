//
//  SelectEffect.swift
//
//
//  Created by Arnaud VERRIER on 10/03/2024.
//

import Foundation
import SwiftUI

public struct PressEffect: ViewModifier {
    public var action: PressCursorAction
    
    public init(action: PressCursorAction) {
        self.action = action
    }

    public func body(content: Content) -> some View {
        content
            .scaleEffect(action == .release ? 1 : 1.1)
            .animation(.easeIn, value: action)
            .shadow(color: .yellow, radius: action == .release ? 0 : 10)
            .animation(.easeIn, value: action)
    }
}
