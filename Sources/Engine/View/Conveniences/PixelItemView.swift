//
//  File.swift
//  
//
//  Created by Arnaud VERRIER on 03/03/2024.
//

import Board
import Foundation
import SwiftUI

public struct PixelItemView<Content> : View where Content : View {
    @EnvironmentObject
    var cursorService: CursorEnvironnementService
    @EnvironmentObject
    var actionService: ActionnerEnvironnementService
    
    let pixel: Pixel
    
    var content: () -> Content
    
    var identier: String
    
    public init(identier: String, pixel: Pixel, @ViewBuilder content: @escaping () -> Content) {
        self.identier = identier
        self.pixel = pixel
        self.content = content
    }
    
    public var body: some View {
        GeometryReader { geo in
            SubPixelItemView(
                pixel: pixel,
                cursorListener: cursorService.registerListener(on: geo.frame(in: .global))
            ) { value in
                ZStack {
                    content()
                    PixelView(pixel: pixel)
                }
                .onChange(of: value) { oldValue, newValue in
                    pixel.highlightRegionId(newValue)
                    actionService.dispatch(action: "Pixel:\(newValue):\(identier)")
                }
            }
        }
    }
}

struct SubPixelItemView<Content> : View where Content : View {
    @ObservedObject
    var cursorListener: FullCursorListener
    
    let pixel: Pixel
    
    var content: (String) -> Content
    
    init(
        pixel: Pixel,
        cursorListener: FullCursorListener,
        @ViewBuilder content: @escaping (String) -> Content
    ) {
        self.pixel = pixel
        self.content = content
        self._cursorListener = ObservedObject(initialValue: cursorListener)
    }
    
    var body: some View {
        content(regionForLocation(cursorListener: cursorListener))
    }
    
    func regionForLocation(cursorListener: FullCursorListener) -> String{
        switch cursorListener.cursorLocation {
        case .dragOn(let cursorLocation):
            return pixel.getRegionForLocation(cursorLocation, in: cursorListener.frame.size)
        case .pressOn(let cursorLocation):
            return pixel.getRegionForLocation(cursorLocation, in: cursorListener.frame.size)
        default:
            return "NoRegion"
        }
    }
}

extension Pixel {
    func getRegionForLocation(_ location: CGPoint, in size: CGSize) -> String {
        switch state {
        case .associate(let id):
            return id ?? "No region id"
        case .splited(let pixels):
            let pixelLocation = PixelLocation(location: location, in: size)
            let pixel = pixels.first { $0.name == self.name + pixelLocation.rawValue }
            let newSize = CGSize(width: size.width/2, height: size.height/2)
            let newLocation = pixelLocation.projection(location, size: newSize)
            return pixel!.getRegionForLocation(newLocation, in: newSize)
        }
    }
}

enum PixelLocation: String {
    case topLeft = "0"
    case topRight = "1"
    case bottomLeft = "2"
    case bottomRight = "3"
    
    init(location: CGPoint, in size: CGSize) {
        let isTop = location.y < size.height / 2
        let isLeft = location.x < size.width / 2
        
        switch (isTop, isLeft) {
        case (true, true): self = .topLeft
        case (true, false): self = .topRight
        case (false, false): self = .bottomRight
        case (false, true): self = .bottomLeft
        }
    }
    
    func projection(_ location: CGPoint, size: CGSize) -> CGPoint {
        switch self {
        case .topLeft:
            return location
        case .topRight:
            return CGPoint(x: location.x - size.width, y: location.y)
        case .bottomRight:
            return CGPoint(x: location.x - size.width, y: location.y - size.height)
        case .bottomLeft:
            return CGPoint(x: location.x, y: location.y - size.height)
        }
    }
}
