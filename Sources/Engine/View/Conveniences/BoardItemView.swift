//
//  BoardItemView.swift
//
//
//  Created by Arnaud VERRIER on 04/03/2024.
//

import Board
import Foundation
import SwiftUI

public struct BoardItemView<Content> : View where Content : View {
    @EnvironmentObject
    var cursorService: CursorEnvironnementService
    @EnvironmentObject
    var actionService: ActionnerEnvironnementService
    
    var content: () -> Content
    
    var identifier: String
    
    public init(identifier: String, @ViewBuilder content: @escaping () -> Content) {
        self.identifier = identifier
        self.content = content
    }
    
    public var body: some View {
        GeometryReader { geo in
            SubBoardItemView(
                cursorListener: cursorService.registerListener(on: geo.frame(in: .global))
            ) { action in
                content()
                    .modifier(DragEffect(action: action))
                    .onChange(of: action) { oldValue, newValue in
                        actionService.dispatch(action: "Board:\(newValue):\(identifier)")
                    }
            }
        }
    }
}

struct SubBoardItemView<Content> : View where Content : View {
    @ObservedObject
    var cursorListener: RecievedCursorListener
    
    var content: (RecievedCursorAction) -> Content
    
    init(
        cursorListener: RecievedCursorListener,
        @ViewBuilder content: @escaping (RecievedCursorAction) -> Content
    ) {
        self.content = content
        self.cursorListener = cursorListener
    }
    
    var body: some View {
        content(cursorListener.cursorLocation)
    }
}
