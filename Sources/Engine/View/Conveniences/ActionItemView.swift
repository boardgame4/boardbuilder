//
//  File.swift
//  
//
//  Created by Arnaud VERRIER on 04/03/2024.
//

import Board
import Foundation
import SwiftUI

public struct ActionItemView<Content> : View where Content : View {
    @EnvironmentObject
    var cursorService: CursorEnvironnementService
    @EnvironmentObject
    var actionService: ActionnerEnvironnementService
    
    var content: () -> Content
    
    var identifier: String
    
    public init(identifier: String, @ViewBuilder content: @escaping () -> Content) {
        self.identifier = identifier
        self.content = content
    }
    
    public var body: some View {
        GeometryReader { geo in
            SubActionItemView(
                cursorListener: cursorService.registerListener(on: geo.frame(in: .global))
            ) { action in
                content()
                    .modifier(PressEffect(action: action))
                    .onChange(of: action) { _, newValue in
                        actionService.dispatch(action: "Action:\(newValue):\(identifier)")
                    }
            }
        }
    }
}

struct SubActionItemView<Content> : View where Content : View {
    
    var content: (PressCursorAction) -> Content
    
    @ObservedObject
    var cursorListener: PressCursorListener
    
    init(
        cursorListener: PressCursorListener,
        @ViewBuilder content: @escaping (PressCursorAction) -> Content
    ) {
        self.content = content
        self._cursorListener = ObservedObject(initialValue: cursorListener)
    }
    
    var body: some View {
        content(cursorListener.cursorLocation)
    }
}
