//
//  SwiftUIView.swift
//  
//
//  Created by Arnaud VERRIER on 03/03/2024.
//

import Board
import SwiftUI

#Preview {
    GlobalOverlayView(actionnerRegistrar: ActionnerRegistrarService()) {
        HStack {
            BoardActionView()
            BoardView()
        }
        .environmentObject(RegionFactory())
    }
}

fileprivate struct BoardView: View {
    let pixel = Pixel(
        name: "0",
        state: PixelState.splited(
            pixels: [
                Pixel(name: "00", state: .associate(id: "0")),
                Pixel(name: "01", state: .associate(id: "1")),
                Pixel(name: "02", state: .associate(id: "2")),
                Pixel(name: "03", state: .associate(id: "3"))
            ]
        )
    )
    
    var body: some View {
        PixelItemView(identier: "Board", pixel: pixel) {
            Rectangle().fill(.yellow)
        }
    }
}

fileprivate struct BoardActionView: View {
    var body: some View {
        ActionItemView(identifier: "BoardAction") {
            Rectangle().fill(.blue)
        }
    }
}

fileprivate extension PressCursorAction {
    var color: Color {
        switch self {
        case .release:
            return .blue
        case .pressed:
            return .green
        }
    }
}

