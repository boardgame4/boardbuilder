//
//  DragEffectTestView.swift
//
//
//  Created by Arnaud VERRIER on 03/03/2024.
//

import Board
import SwiftUI

#Preview {
    GlobalOverlayView(
        actionnerRegistrar: ActionnerRegistrarService()) {
        HStack {
            DragEffectTestView()
        }
    }
}

struct DragEffectTestView: View {
    
    var columns: [GridItem] = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var cards = Array(0...399)
    
    var body: some View {
        GeometryReader { geo in
            ScrollView {
                // 4. Populate into grid
                LazyVGrid(columns: columns, spacing: 0) {
                    ForEach(cards, id: \.self) { card in
                        CardView(identifier: "card\(card)")
                            .frame(
                                width: geo.size.width/CGFloat(columns.count),
                                height: geo.size.width/CGFloat(columns.count))
                    }
                }.background(
                    .linearGradient(
                        .init(
                            .init(
                                colors: [.brown, .cyan]
                            )
                        ),
                        startPoint: .leading,
                        endPoint: .trailing
                    )
                )
            }.background(.yellow)
            .padding()
        }
    }
}

struct CardView: View {
    var identifier: String
    
    var body: some View {
        ActionItemView(identifier: identifier) {
            BoardItemView(identifier: identifier) {
                GeometryReader { geo in
                    Rectangle()
                        .fill(.red)
                }
            }
        }
    }
}
