//
//  ActionnerEnvironnementService.swift
//
//
//  Created by Arnaud VERRIER on 07/03/2024.
//

import Combine
import Foundation
import SwiftUI

final class ActionnerEnvironnementService: ObservableObject {
    var actionnerRegistrar: ActionnerRegistrar
    
    func dispatch(action: String) {
        print(action)
        actionnerRegistrar.executeActionnerFor(action)
    }
    
    init(actionnerRegistrar: ActionnerRegistrar) {
        self.actionnerRegistrar = actionnerRegistrar
    }
}

public final class ActionnerRegistrarService: ActionnerRegistrar {
    public var actionners: [Actionner] = []
    
    public func register(_ actionner: Actionner) {
        actionners.append(actionner)
    }
    
    public func executeActionnerFor(_ identifier: String) {
        actionners.first { $0.identifier == identifier }?.execution()
    }
    
    public init() {}
}

public protocol ActionnerRegistrar {
    var actionners: [Actionner] { get }
    
    func register(_ actionner: Actionner)
    
    func executeActionnerFor(_ identifier: String)
}

public protocol Actionner {
    var identifier: String { get }
    
    func execution()
}
