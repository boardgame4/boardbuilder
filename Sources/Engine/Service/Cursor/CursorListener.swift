//
//  CursorListener.swift
//
//
//  Created by Arnaud VERRIER on 10/03/2024.
//

import Foundation

protocol CursorListener {
    func setLocation(location: CGPoint?)
    
    init(frame: CGRect)
}
