//
//  FullCursorListener.swift
//
//
//  Created by Arnaud VERRIER on 10/03/2024.
//

import Combine
import Foundation

public enum FullCursorAction: Equatable {
    case dragOn(cursorLocation: CGPoint)
    case pressOn(cursorLocation: CGPoint)
    case outside
    case release
}

class FullCursorListener: CursorListener, ObservableObject {
    var frame: CGRect
    
    @Published
    var cursorLocation: FullCursorAction = .release
    
    required init(frame: CGRect) {
        self.frame = frame
    }
    
    func setLocation(location: CGPoint?) {
        guard let location else {
            cursorLocation = .release
            return
        }
        
        guard frame.contains(location) else {
            cursorLocation = .outside
            return
        }
        
        switch cursorLocation {
        case .release:
            cursorLocation = .pressOn(
                cursorLocation: CGPoint(
                    x: location.x - frame.origin.x,
                    y: location.y - frame.origin.y
                )
            )
        default:
            cursorLocation = .dragOn(
                cursorLocation: CGPoint(
                    x: location.x - frame.origin.x,
                    y: location.y - frame.origin.y
                )
            )
        }
    }
}
