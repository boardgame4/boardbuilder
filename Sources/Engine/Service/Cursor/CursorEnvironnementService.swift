//
//  CursorService.swift
//
//
//  Created by Arnaud VERRIER on 07/03/2024.
//

import Combine
import Foundation

class CursorEnvironnementService: ObservableObject {
    @Published
    var cursorLocation: CGPoint?
    
    // Needed to avoid invalidate body of view that need CursorEnvironnementService. 
    // Use CursorListener to invalidate your body if needed.
    var objectWillChange = PassthroughSubject<Void, Never>()
    
    var bags = Set<AnyCancellable>()
    
    func registerListener<CL: CursorListener>(on frame: CGRect) -> CL {
        let cursorListener = CL(frame: frame)
        
        $cursorLocation.sink { location in
            cursorListener.setLocation(location: location)
        }.store(in: &bags)

        return cursorListener
    }
    
    func registerActionner() -> CursorActionner {
        let cursorActionner = CursorActionner()
        cursorActionner.$cursorLocation
            .dropFirst()
            .sink { location in
            self.cursorLocation = location
        }.store(in: &bags)
        return cursorActionner
    }
}

class CursorActionner: ObservableObject {
    @Published
    var cursorLocation: CGPoint?
}
