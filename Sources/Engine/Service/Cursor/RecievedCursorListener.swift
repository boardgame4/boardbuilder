//
//  FullCursorListener.swift
//
//
//  Created by Arnaud VERRIER on 10/03/2024.
//

import Combine
import Foundation

public enum RecievedCursorAction: Equatable {
    case dragOn
    case none
}

class RecievedCursorListener: CursorListener, ObservableObject {
    var frame: CGRect
    
    @Published
    var cursorLocation: RecievedCursorAction = .none
    
    required init(frame: CGRect) {
        self.frame = frame
    }
    
    func setLocation(location: CGPoint?) {
        guard let location else {
            cursorLocation = .none
            return
        }
        
        guard frame.contains(location) else {
            cursorLocation = .none
            return
        }
        
        cursorLocation = .dragOn
    }
}
