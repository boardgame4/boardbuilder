//
//  FullCursorListener.swift
//
//
//  Created by Arnaud VERRIER on 10/03/2024.
//

import Combine
import Foundation

public enum PressCursorAction: Equatable {
    case pressed
    case release
}

class PressCursorListener: CursorListener, ObservableObject {
    var frame: CGRect
    
    @Published
    var cursorLocation: PressCursorAction = .release
    
    var isDraggIn: Bool = false
    
    required init(frame: CGRect) {
        self.frame = frame
    }
    
    func setLocation(location: CGPoint?) {
        guard let location else {
            isDraggIn = false
            cursorLocation = .release
            return
        }
        
        if isDraggIn == false && frame.contains(location) {
            cursorLocation = .pressed
        }
        
        isDraggIn = true
    }
}
