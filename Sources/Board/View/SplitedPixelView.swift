//
//  SwiftUIView 2.swift
//  
//
//  Created by Arnaud VERRIER on 03/02/2024.
//

import SwiftUI

struct SplitedPixelView: View {
    
    var pixels: [Pixel]
    
    var parent: Pixel?
    
    var onTap: ((String) -> PixelAction)?
    
    var onLongPress: ((String) -> PixelLongAction)?
    
    var body: some View {
        GeometryReader { geo in
            VStack(spacing: 0) {
                HStack(spacing: 0) {
                    PixelView(pixel: pixels[0], parent: parent, onTap: onTap, onLongPress: onLongPress)
                        .frame(
                            width: geo.size.width/2,
                            height: geo.size.height/2
                        )
                    PixelView(pixel: pixels[1], parent: parent, onTap: onTap, onLongPress: onLongPress)
                        .frame(
                            width: geo.size.width/2,
                            height: geo.size.height/2
                        )
                }
                HStack(spacing: 0) {
                    PixelView(pixel: pixels[2], parent: parent, onTap: onTap, onLongPress: onLongPress)
                        .frame(
                            width: geo.size.width/2,
                            height: geo.size.height/2
                        )
                    PixelView(pixel: pixels[3], parent: parent, onTap: onTap, onLongPress: onLongPress)
                        .frame(
                            width: geo.size.width/2,
                            height: geo.size.height/2
                        )
                }
            }
        }
    }
}

#Preview {
    SplitedPixelView(
        pixels: [
            Pixel(name: "0", state: .associate(id: nil)),
            Pixel(name: "1", state: .associate(id: nil)),
            Pixel(name: "2", state: .associate(id: nil)),
            Pixel(name: "3", state: .associate(id: nil))
        ]
    )
    .environmentObject(RegionFactory())
}
