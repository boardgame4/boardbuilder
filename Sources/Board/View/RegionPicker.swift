//
//  SwiftUIView.swift
//  
//
//  Created by Arnaud VERRIER on 06/02/2024.
//

import SwiftUI

/// A view to select a Region
public struct RegionPicker: View {
    @EnvironmentObject
    var regionFactory: RegionFactory
    
    /// The current selected region
    @Binding public var currentRegion: String?
    
    @Binding public var usedRegionIds: [String]
    
    /// If false, reduce the opacity
    public var focusOn: Bool {
        currentRegion != nil
    }
    
    /// Build a view to select a region
    /// - Parameters:
    ///   - currentRegion: the selected region.
    ///   - usedRegionIds: the list of already used regionId
    public init(
        currentRegion: Binding<String?>,
        usedRegionIds: Binding<[String]>
    ) {
        self._currentRegion = currentRegion
        self._usedRegionIds = usedRegionIds
    }
    
    var columns: [GridItem] = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    public var body: some View {
        ScrollView {
            LazyVGrid(columns: columns) {
                ForEach(regionFactory.regionIds, id: \.self) { regionId in
                    HStack {
                        Text(regionId)
                        Spacer()
                        Rectangle()
                            .fill(regionFactory.colorsForIdOrDefault(regionId))
                            .frame(width: 40)
                    }
                    .opacity(usedRegionIds.contains(regionId) || currentRegion == regionId ? 1 : 0.4)
                    .onTapGesture {
                        currentRegion = regionId
                        if usedRegionIds.contains(regionId) == false {
                            usedRegionIds.append(regionId)
                        }
                    }
                    .padding(EdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4))
                    .border(focusOn && currentRegion == regionId ? .black : .clear)
                }
            }
        }
        .opacity(focusOn ? 1 : 0.4)
    }
}
