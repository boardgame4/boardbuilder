//
//  SwiftUIView 2.swift
//  
//
//  Created by Arnaud VERRIER on 02/02/2024.
//

import SwiftUI

public struct PixelView: View {
    @EnvironmentObject
    var regionFactory: RegionFactory
    
    @ObservedObject
    var pixel: Pixel
    
    var parent: Pixel?
    
    var onTap: ((String) -> PixelAction)?
    
    var onLongPress: ((String) -> PixelLongAction)?
    
    public init(
        pixel: Pixel,
        parent: Pixel? = nil,
        onTap: ((String) -> PixelAction)? = nil,
        onLongPress: ((String) -> PixelLongAction)? = nil
    ) {
        self.pixel = pixel
        self.parent = parent
        self.onTap = onTap
        self.onLongPress = onLongPress
    }
    
    public var body: some View {
        switch pixel.state {
        case .associate(let id):
            Rectangle()
                .fill(regionFactory.colorsForIdOrDefault(id))
                .opacity(id == nil ? 0.00001 : 0.5)
                .border(id == nil || pixel.highlighted ? .black : .clear, width: 0.5)
                .onTapGesture {
                    switch onTap?(pixel.name) {
                    case .none:
                        break
                    case .some(let result):
                        switch result {
                        case .split:
                            withAnimation {
                                pixel.split()
                            }
                        case .addToRegion(let id):
                            withAnimation {
                                pixel.associateId(id)
                            }
                        }
                    }
                }
                .onLongPressGesture {
                    switch onLongPress?(pixel.name) {
                    case .none:
                        break
                    case .some(let result):
                        switch result {
                        case .unsplit:
                            withAnimation {
                                parent?.unsplit()
                            }
                        case .removeFromRegion:
                            withAnimation {
                                pixel.associateId(nil)
                            }
                        }
                    }
                }
        case .splited(let pixels):
            SplitedPixelView(
                pixels: pixels,
                parent: pixel,
                onTap: onTap,
                onLongPress: onLongPress
            )
        }
    }
}

#Preview {
    PixelView(pixel: Pixel(name: "", state: .associate(id: nil)))
        .environmentObject(RegionFactory())
}

#Preview {
    PixelView(pixel: Pixel(name: "", state: .splited(pixels: [])))
        .environmentObject(RegionFactory())
}
