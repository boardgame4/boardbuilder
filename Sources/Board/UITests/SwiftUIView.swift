//
//  SwiftUIView.swift
//  
//
//  Created by Arnaud VERRIER on 02/02/2024.
//

import SwiftUI

fileprivate struct SwiftUIView: View {
    var body: some View {
        ZStack {
            BoardView()
            PixelView(pixel: Pixel(name: "0", state: .associate(id: nil)))
        }
        .frame(width: 500, height: 500)
    }
}

fileprivate struct BoardView: View {
    var body: some View {
        Image(
            "board",
            bundle: .module
        ).resizable(resizingMode: .stretch)
    }
}

#Preview {
    SwiftUIView()
}
