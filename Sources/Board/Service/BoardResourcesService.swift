//
//  FileManager.swift
//  BoardCreator
//
//  Created by Arnaud VERRIER on 13/02/2024.
//

import Foundation

enum FileManagerError: Error {
    case technicalImplementation(String)
}

public struct BoardResource {
    public let name: String
    
    public let data: Data
    
    public init(name: String, data: Data) {
        self.name = name
        self.data = data
    }
}

public final class BoardResourcesService {
    private let bundle = Bundle.main
    
    static let boardIdentifier = "_board.json"
    
    public init() {}
    
    public func getBoardResources() throws -> [BoardResource] {
        var boardResources = [BoardResource]()
        for board in try getBoards() {
            let data = try load(name: board)
            boardResources.append(
                BoardResource(
                    name: board.replacingOccurrences(of: Self.boardIdentifier, with: ""),
                    data: data
                )
            )
        }
        return boardResources
    }
    
    public func load(name: String) throws -> Data {
        guard let fileUrl = bundle.url(forResource: name, withExtension: nil) else {
            print("JSON file (\(name)) not found in bundle.")
            throw FileManagerError.technicalImplementation("JSON file \"\(name)\"not found in bundle.")
        }
        let data = try Data(contentsOf: fileUrl)
        return data
    }
    
    private func getBoards() throws -> [String] {
        let files = try FileManager.default.contentsOfDirectory(atPath: Bundle.main.resourcePath!)
        return files.filter { $0.contains(Self.boardIdentifier) }
    }
}
