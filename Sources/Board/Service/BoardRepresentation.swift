//
//  File.swift
//  
//
//  Created by Arnaud VERRIER on 04/02/2024.
//

import Foundation
import SwiftUI

/// An action perfom on a pixel after a tap gesture
public enum PixelAction {
    /// split a Pixel into 4 Pixels
    case split
    /// associate the pixel to the region regionId
    case addToRegion(regionId: String)
}

/// An action perfom on a pixel after a long press gesture
public enum PixelLongAction {
    ///  Cancel the split done on parent Pixel
    case unsplit
    /// de-associate the pixel to its region
    case removeFromRegion
}

/// The entry point to build the Pixels representation of the board game.
/// Use @viewBuilder view to add the representation to your view.
/// At init, the board contains only one Pixel that can be splited.
/// onTap, onLongPress return the action responsibility to the board creator
public class BoardRepresentation: ObservableObject {
    @Published
    var firstPixel: Pixel
    
    public let name: String
    
    var onTap: ((String) -> PixelAction)?
    
    var onLongPress: ((String) -> PixelLongAction)?
    
    /// Build a single Pixel view as starting point for your board representation
    /// - Parameters:
    ///   - name: unique name for the board
    ///   - onTap: Return a ``PixelAction`` when the user tap on the named pixel
    ///   - onLongPress: Return a ``PixelAction`` when the user long press on the named pixel
    public init(
        name: String,
        onTap: ((String) -> PixelAction)?,
        onLongPress: ((String) -> PixelLongAction)?
    ) {
        self.name = name
        self.firstPixel = Pixel(name: "0")
        self.onTap = onTap
        self.onLongPress = onLongPress
    }
    
    init(firstPixel: Pixel, name: String) {
        self.name = name
        self.firstPixel = firstPixel
    }
    
    /// The list of used regionIds
    public var regionIds: [String] {
        firstPixel.regionIds
    }
    
    /// Hightligh all pixel that belong to a specifique region
    /// - Parameter regionId: Unique identifiant for the targeted region
    public func highlightRegionId(_ regionId: String?) {
        firstPixel.highlightRegionId(regionId)
    }
    
    /// Use encodeToData for saving the current topology of the board
    /// - Returns: encoded current topology of the board
    public func encodeToData() throws -> Data {
        let encoder = JSONEncoder()
        let data = try encoder.encode(firstPixel)
        return data
    }
    
    /// Use loadFromData for loading a saved topology of the board
    /// - Parameter data: encoded current topology of the board
    public func loadFromData(_ data: Data) throws {
        let jsonDecoder = JSONDecoder()
        let firstPixel = try jsonDecoder.decode(Pixel.self, from: data)
        self.firstPixel = firstPixel
    }
    
    @ViewBuilder
    /// A pixel representation of the Board
    public var view: some View {
        PixelView(pixel: firstPixel, parent: nil, onTap: onTap, onLongPress: onLongPress)
    }
}
