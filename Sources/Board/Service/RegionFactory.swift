import Combine
import SwiftUI

/// A Environnement object used as connection point between Application and BoardRepresentation
/// RegionFactory has the responsiblilty to define the available region
public class RegionFactory: ObservableObject {
    /// The list of available region to create the board
    public let regionIds: [String]
    
    private let colors: [String : Color]
    
    /// Create the service with a 37 available region
    public init() {
        self.colors = RegionFactory.defaultColors
        self.regionIds = Array(0..<colors.count).map { "\($0)" }
    }
    
    static var defaultColors: [String: Color] = [
        "0": .red,
        "1": .green,
        "2": .blue,
        "3": .yellow,
        "4": .orange,
        "5": .purple,
        "6": .pink,
        "7": .primary,
        "8": .secondary,
        "9": .accentColor,
        "10": Color(red: 0.5, green: 0.5, blue: 0.5),
        "11": Color(red: 0.2, green: 0.8, blue: 0.4),
        "12": Color(red: 0.7, green: 0.2, blue: 0.9),
        "13": Color(red: 0.9, green: 0.5, blue: 0.1),
        "14": Color(red: 0.1, green: 0.7, blue: 0.9),
        "15": Color(red: 0.8, green: 0.3, blue: 0.4),
        "16": Color(red: 0.3, green: 0.6, blue: 0.2),
        "17": Color(red: 0.6, green: 0.4, blue: 0.8),
        "18": Color(red: 0.4, green: 0.9, blue: 0.7),
        "19": Color(red: 0.2, green: 0.4, blue: 0.7),
        "20": Color(red: 0.7, green: 0.9, blue: 0.3),
        "21": Color(red: 0.5, green: 0.3, blue: 0.8),
        "22": Color(red: 0.8, green: 0.7, blue: 0.2),
        "23": Color(red: 0.1, green: 0.5, blue: 0.6),
        "24": Color(red: 0.9, green: 0.2, blue: 0.5),
        "25": Color(red: 0.3, green: 0.8, blue: 0.1),
        "26": Color(red: 0.6, green: 0.1, blue: 0.5),
        "27": Color(red: 0.4, green: 0.5, blue: 0.9),
        "28": Color(red: 0.7, green: 0.4, blue: 0.1),
        "29": Color(red: 0.8, green: 0.1, blue: 0.7),
        "30": Color(red: 0.2, green: 0.6, blue: 0.9),
        "31": Color(red: 0.5, green: 0.9, blue: 0.4),
        "32": Color(red: 0.9, green: 0.6, blue: 0.3),
        "33": Color(red: 0.3, green: 0.2, blue: 0.6),
        "34": Color(red: 0.6, green: 0.7, blue: 0.8),
        "35": Color(red: 0.4, green: 0.8, blue: 0.5),
        "36": Color(red: 0.7, green: 0.5, blue: 0.2)
    ]
    
    /// Get a color for a regionId
    /// - Parameter id: The unique identifiant of the region
    /// - Returns: The associate color for the region
    public func colorsForIdOrDefault(_ id: String?) -> Color {
        if let id {
            return colors[id] ?? .white
        } else {
            return .white
        }
    }
}
