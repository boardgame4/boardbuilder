//
//  Pixel.swift
//
//
//  Created by Arnaud VERRIER on 04/02/2024.
//

import Foundation

public enum PixelState {
    case associate(id: String?)
    case splited(pixels: [Pixel])
}

enum PixelError: Error {
    case decodeErrorDoubleState
    case loadWithEmpyData
}

public class Pixel: Codable, ObservableObject {
    public let name: String
    
    @Published
    public var state: PixelState
    
    @Published var highlighted: Bool = false
    
    public init(name: String, state: PixelState = .associate(id: nil)) {
        self.name = name
        self.state = state
    }
    
    lazy var child0Name = name + "0"
    lazy var child1Name = name + "1"
    lazy var child2Name = name + "2"
    lazy var child3Name = name + "3"
    
    var regionIds: [String] {
        switch state {
        case .associate(let id):
            if let id {
                return [id]
            }
            return []
        case .splited(let pixels):
            return pixels.flatMap { $0.regionIds }
        }
    }
    
    func split() {
        self.state = .splited(
            pixels: [
                Pixel(name: child0Name, state: .associate(id: nil)),
                Pixel(name: child1Name, state: .associate(id: nil)),
                Pixel(name: child2Name, state: .associate(id: nil)),
                Pixel(name: child3Name, state: .associate(id: nil))
            ]
        )
    }
    
    func unsplit() {
        self.state = .associate(id: nil)
    }
    
    func associateId(_ id: String?) {
        self.state = .associate(id: id)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case regionId = "region_id"
        case pixels
    }
    
    public func highlightRegionId(_ regionId: String?) {
        switch state {
        case .associate(let id):
            guard let regionId else {
                highlighted = false
                return
            }
            highlighted = regionId == id
        case .splited(let pixels):
            pixels.forEach { $0.highlightRegionId(regionId) }
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .id)
        switch state {
        case .associate(let id):
            try container.encodeIfPresent(id, forKey: .regionId)
        case .splited(let pixels):
            try container.encode(pixels, forKey: .pixels)
        }
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .id)
        
        let pixels = try container.decodeIfPresent([Pixel].self, forKey: .pixels)
        
        let regionId = try container.decodeIfPresent(String.self, forKey: .regionId)
        
        if pixels != nil && regionId != nil {
            throw PixelError.decodeErrorDoubleState
        }
        
        if let pixels {
            self.state = .splited(pixels: pixels)
        } else {
            if let regionId {
                self.state = .associate(id: regionId)
            } else {
                self.state = .associate(id: nil)
            }
        }
    }
}
